# Docker Medusa

Dockerfile to set up [Medusa](https://pymedusa.com/)

[![medusa](https://camo.githubusercontent.com/89e3583d77e019c4d77db7698c7070b684a7978a/68747470733a2f2f63646e2e7261776769742e636f6d2f70796d65647573612f6d65647573612e6769746875622e696f2f34333630643439342f696d616765732f6c6f676f2f6e65772d6c6f676f2e706e67)](https://github.com/pymedusa/Medusa)

## Repository

Sources are [here](https://gitlab.com/jpiron/docker-medusa)

## Run

```bash
docker pull jpiron/medusa
docker run jpiron/medusa
```

However, you're more likely to run the container with following:

```bash
docker run -d --restart=always --name medusa \
-p 8081:8081 \
--user $(id -u ${USER}):$(id -g ${USER}) \
-e TZ=$(cat /etc/timezone) \
-v <YOUR_MEDUSA_DATA_DIRECTORY>:/medusa/datadir \
-v <YOUR_DOWNLOAD_DIRECTORY>:/downloads \
-v <YOUR_TV_SHOWS_DIRECTORY>:/tv \
jpiron/medusa
```

## Build

```shell
VERSION=v1.0.7 make build
```

```shell
VERSION=v1.0.7 NO_CACHE=1 PULL=1 make build
```

## Parameters

* **user**: Docker run parameter to set the user/group to run the container with;
* **TZ**: the timzeone to run the container with;
* **YOUR_MEDUSA_DATA_DIRECTORY**: if you want Medusa data directory to be hold in a local directory. If the directory contains a config.ini file it will be used to configure Medusa.
* **YOUR_DOWNLOAD_DIRECTORY**: provide Medusa with an access to your download directory. Can be used by post-processing scripts.
* **YOUR_TV_SHOWS_DIRECTORY**: provide Medusa with an access to your tv_shows directory. Can also be used by post-processing scripts.

## Caveats

The Dockerfile creates a Medusa user and makes it the owner of the Medusa installation folder.
When using the **user** parameter, the container is ran with a different user which is not the owner of the Medusa installation folder.
As a result auto-updates can't be performed with the **user** parameter.
To perform updates you will have to pull (or build) the latest image version and run it.

## Versions

* **17.08.2020**: Update default version.
* **26.03.2019**: Add Docker build pull option support.
* **26.03.2019**: Add Docker build no-cache option support.
* **06.03.2019**: First release.
