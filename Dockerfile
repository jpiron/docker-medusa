FROM ubuntu:22.04
LABEL maintainer="jonathan@piron.at"

# Install Medusa requirements
# ca-certificates, git-core and locales are not an actual Medusa requirements are required to properly setup the environment.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        git-core\
        locales \
        mediainfo \
        python3-gdbm \
        python3 \
    && rm -rf /var/lib/apt/lists/*

# Set locale.
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Get Medusa.
ARG VERSION='0.4.0'
RUN git clone --depth 1 --branch "${VERSION}" https://github.com/pymedusa/Medusa.git /medusa

# Create Medusa group and user.
RUN groupadd --system medusa\
    && useradd --system -s /bin/false -d /medusa -g medusa medusa

RUN mkdir /downloads /medusa/datadir /tv\
    && chown -R medusa:medusa /medusa /downloads /tv

VOLUME ["/medusa/datadir", "/downloads", "/tv"]

EXPOSE 8081

USER medusa

ENTRYPOINT ["/usr/bin/python3", "/medusa/start.py"]
CMD ["--datadir=/medusa/datadir/"]
