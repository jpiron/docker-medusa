
VERSION ?= master
DOCKER_VERSION := $(VERSION:master=latest)
DOCKER_OPTS := $(shell [ "${NO_CACHE}" = "1" ] && echo '--no-cache')
DOCKER_OPTS += $(shell [ "${PULL}" = "1" ] && echo '--pull')
REGISTRY_IMAGE ?= jpiron/medusa

.PHONY: all
all: build publish

.PHONY: info
# target: info - Display some useful information about environment and variables used
info:
	@echo -e '\n### INFO ###'
	@echo -e 'version=${VERSION}'
	@echo -e 'docker_version=${DOCKER_VERSION}'
	@echo -e 'docker_opts=${DOCKER_OPTS}'
	@echo -e 'registry_image=${REGISTRY_IMAGE}'

.PHONY: build
# target: build - Build Medusa Docker image
build:
	@echo -e '\n### BUILD ###'
	docker build ${DOCKER_OPTS} \
		-f Dockerfile \
		--build-arg VERSION=$(VERSION) \
		-t $(REGISTRY_IMAGE):$(DOCKER_VERSION) .

.PHONY: publish
# target: publish - Publish Medusa Docker image
publish: docker-login
	@echo -e '\n### PUBLISH ###'
	docker push $(REGISTRY_IMAGE):$(DOCKER_VERSION)

.PHONY: clean
# target: clean - Remove artifacts related to the current version of Medusa
clean:
	@echo -e '\n### CLEAN ###'
	docker images --filter "reference=$(REGISTRY_IMAGE):$(DOCKER_VERSION)" --quiet 2>/dev/null | xargs --no-run-if-empty docker rmi --force

.PHONY: purge
# target: purge - Remove all artifacts
purge: clean
	@echo -e '\n### PURGE ###'
	docker images --filter "reference=$(REGISTRY_IMAGE)" --quiet 2>/dev/null | xargs --no-run-if-empty docker rmi --force

.PHONY: docker-login
docker-login: check-docker-env
	@echo -e '\n### DOCKER LOGIN ###'
	@docker login -u '$(DOCKER_REGISTRY_USER)' -p '$(DOCKER_REGISTRY_PASSWORD)' 2> /dev/null

HELP_SOURCES ?= Makefile*
.PHONY: help
# target: help - Print this help
help:
	@egrep -h "^# target: " $(HELP_SOURCES) \
		| sed -e 's/^# target: //g' \
		| sort -h \
		| awk '{printf("    %-26s", $$1); $$1=$$2=""; print "-" $$0}'

.PHONY: check-docker-env
check-docker-env:
ifndef DOCKER_REGISTRY_USER
	$(error DOCKER_REGISTRY_USER is undefined)
endif
ifndef DOCKER_REGISTRY_PASSWORD
	$(error DOCKER_REGISTRY_PASSWORD is undefined)
endif
